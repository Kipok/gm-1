import numpy as np
from itertools import groupby
import sys


def make_generator_matrix(H):
    """
    This function assumes H is full-rank.
    Returns G, ind
    """
    H_copy = H.copy()
    ind_h = np.empty(H.shape[0], np.int)
    for i in range(H_copy.shape[0]):
        idx = np.where(H_copy[i] == 1)[0][0]
        ind_h[i] = idx
        line = H_copy[i].copy()
        add_lines = np.where(H_copy[:, idx] == 1)[0]
        H_copy[add_lines] = np.bitwise_xor(H_copy[add_lines], line)
        H_copy[i] = line
    G = np.empty((H_copy.shape[1], H_copy.shape[1] - H_copy.shape[0]), np.int)
    ind = np.setdiff1d(np.arange(H_copy.shape[1]), ind_h)
    G[ind] = np.eye(G.shape[1])
    G[ind_h] = H_copy[:,ind]
    return G, ind


def generate_h(m, n, num_ones=3):
    H = np.zeros((m, n), np.int)
    for i in range(m):
        ind = np.random.choice(n, num_ones)
        H[i, ind] = 1
    for j in range(n):
        ind = np.random.choice(m, num_ones)
        H[ind, j] = 1
    return H


def decode(s, H, q, schedule='parallel', damping=1,
           max_iter=300, tol_beliefs=1e-4, display=False):
    m, n = H.shape
    mu_eh = np.zeros((n, m, 2))
    mu_he = np.zeros((m, n, 2))
    mu_eh_prev = np.zeros((n, m, 2))
    mu_he_prev = np.zeros((m, n, 2))
    beliefs = np.zeros((n, 2))
    beliefs_old = beliefs.copy()
    mu_eh[:, :, 0] = 1 - q
    mu_eh[:, :, 1] = 1 - mu_eh[:, :, 0]
    for c in range(max_iter):
        if schedule == 'parallel':
            dp = mu_eh[:, :, 0] - mu_eh[:, :, 1]
            for j, i in zip(*np.where(H == 1)):
                ind = np.setdiff1d(np.where(H[j] == 1)[0], np.array(i))
                dpl = np.prod(dp[ind, j])
                mu_he[j, i, s[j]] = (1.0 + dpl) / 2.0
                mu_he[j, i, s[j] ^ 1] = (1.0 - dpl) / 2.0
                mu_he[j, i] /= np.sum(mu_he[j, i])
            mu_he = damping * mu_he + (1 - damping) * mu_he_prev
            mu_he_prev = mu_he.copy()
            for j, i in zip(*np.where(H == 1)):
                ind = np.setdiff1d(np.where(H[:, i] == 1)[0], np.array(j))
                mu_eh[i, j, 0] = (1 - q) * np.prod(mu_he[ind, i, 0])
                mu_eh[i, j, 1] = q * np.prod(mu_he[ind, i, 1])
                mu_eh[i, j] /= np.sum(mu_eh[i, j])
            mu_eh = damping * mu_eh + (1 - damping) * mu_eh_prev
            mu_eh_prev = mu_eh.copy()
        else:
            data = zip(*np.where(H.T == 1))
            vertices = [list(g) for _, g in groupby(data, lambda x: x[0])]
            for v in vertices:
                dp = mu_eh[:, :, 0] - mu_eh[:, :, 1]
                for i, j in v:
                    ind = np.setdiff1d(np.where(H[j] == 1)[0], np.array(i))
                    dpl = np.prod(dp[ind, j])
                    mu_he[j, i, s[j]] = (1.0 + dpl) / 2.0
                    mu_he[j, i, s[j] ^ 1] = (1.0 - dpl) / 2.0
                    mu_he[j, i] /= np.sum(mu_he[j, i])
                    mu_he[j, i] = damping * mu_he[j, i] + (1 - damping) * mu_he_prev[j, i]
                    mu_he_prev[j, i] = mu_he[j, i]
                for i, j in v:
                    ind = np.setdiff1d(np.where(H[:, i] == 1)[0], np.array(j))
                    mu_eh[i, j, 0] = (1 - q) * np.prod(mu_he[ind, i, 0])
                    mu_eh[i, j, 1] = q * np.prod(mu_he[ind, i, 1])
                    mu_eh[i, j] /= np.sum(mu_eh[i, j])
                    mu_eh[i, j] = damping * mu_eh[i, j] + (1 - damping) * mu_eh_prev[i, j]
                    mu_eh_prev[i, j] = mu_eh[i, j]
        for i in range(n):
            ind = np.where(H[:, i] == 1)[0]
            beliefs[i, 0] = (1 - q) * np.prod(mu_he[ind, i, 0])
            beliefs[i, 1] = q * np.prod(mu_he[ind, i, 1])
            beliefs[i] /= np.sum(beliefs[i])
        e_est = np.argmax(beliefs, 1)
        beliefs_stabilized = np.sum(np.logical_and(
            np.abs(beliefs[:, 0] - beliefs_old[:, 0]) < tol_beliefs,
            np.abs(beliefs[:, 1] - beliefs_old[:, 1]) < tol_beliefs))

        if display:
            print("Iteration #{}: beliefs stabilized {}/{}".format(c, beliefs_stabilized, n))
            sys.stdout.flush()
        if np.allclose(np.dot(H, e_est) % 2, s):
            return e_est, 0
        if np.allclose(beliefs_old, beliefs, atol=tol_beliefs):
            return e_est, 1
        beliefs_old = beliefs.copy()
    return e_est, 2


def estimate_errors(H, q, num_points=200):
    err_bit = np.zeros(H.shape[1])
    err_block = 0
    diver = 0
    good_num = 0
    for _ in range(num_points):
        e = np.random.choice(2, H.shape[1], p=[1-q, q])
        s = np.dot(H, e) % 2
        e_est, status = decode(s, H, q, max_iter=100)
        if status == 2:
            diver += 1
            continue
        good_num += 1
        err_bit += np.bitwise_xor(e, e_est)
        err_block += not np.allclose(e, e_est)
    err_bit = np.mean(err_bit)
    return err_bit / good_num, err_block / good_num, diver / num_points
